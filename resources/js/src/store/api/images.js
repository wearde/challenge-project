import {fetch} from '../../api';

export default {
    loadImages(data) {
        return new Promise((resolve, reject) => {
            fetch(data, resolve, reject);
        });
    },
    add(data) {
        return new Promise((resolve, reject) => {
            fetch(data, resolve, reject);
        });
    },
    removeImage(data) {
        return new Promise((resolve, reject) => {
            fetch(data, resolve, reject);
        });
    },
};
