<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\ReadModel;

use Framework\Db\DBInterface;
use Illuminate\Support\Collection;

class TaskReadRepository
{
  /**
   * @var DBInterface
   */
  private $DB;

  public function __construct(DBInterface $DB)
  {
    $this->DB = $DB;
  }

  public function all($userId, $limit = null): Collection
  {
    return $this->DB->connection()
      ->table('user_tasks')
      ->where('user_id', $userId)
      ->limit($limit)
      ->get();
  }
}