<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Services;

use App\Core\Config;
use App\Entities\User;
use App\Entities\UserImage;
use App\Forms\LoginForm;
use App\Forms\PhotoForm;
use App\Forms\RegisterForm;
use App\Helpers\Auth;
use App\ReadModel\UserReadRepository;
use Framework\Db\DBInterface;
use Illuminate\Support\Collection;
use Psr\Http\Message\UploadedFileInterface;
use Zend\Session\Container;

class AuthService
{
  public const ERROR_KEY = 'error_login';
  /**
   * @var UserReadRepository
   */
  private $repository;
  /**
   * @var Container
   */
  private $session;
  /**
   * @var DBInterface
   */
  private $DB;
  /**
   * @var Config
   */
  private $config;

  public function __construct(
    UserReadRepository $repository,
    Container $session,
    DBInterface $DB,
    Config $config
  )
  {
    $this->repository = $repository;
    $this->session = $session;
    $this->DB = $DB;
    $this->config = $config;
  }

  public function auth(LoginForm $form): ?User
  {
    $user = new User($this->repository->findByUsername($form->login));
    if (!$user || !$user->validatePassword($form->password)) {
      $this->session->offsetSet(self::ERROR_KEY, 'Undefined email or password.');
      throw new \DomainException('Undefined email or password.');
    }
    return $user;
  }

  public function register(RegisterForm $form): bool
  {
    $user = new User(new Collection([
      [
        'username' => $form->username,
        'email' => $form->email,
        'password' => Auth::generate($form->password)
      ]
    ]), $this->DB->connection());

    $user->image = $form->image;

    try {
      $user->save(function () use ($user) {
        $this->DB->connection()->table('users')->insert($user->attributes);
        if ($user->image instanceof UploadedFileInterface && $user->image->getSize()) {
          $userId = $this->DB->getCapture()->getConnection()->getPdo()->lastInsertId();
          $photo = new UserImage($this->config, $this->DB->connection());
          $photo->insert(new PhotoForm($user->image, (int)$userId));
        }
      });
      return true;
    } catch (\Throwable $exception) {
      return false;
    }
  }

  public function uniqUsername(RegisterForm $form): bool
  {
    if ($this->repository->findByUsername($form->username)->first()) {
      return true;
    }
    return false;
  }

  public function uniqEmail($form)
  {
    if ($this->repository->findByEmail($form->email)->first()) {
      return true;
    }
    return false;
  }
}