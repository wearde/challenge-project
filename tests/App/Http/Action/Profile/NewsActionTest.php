<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 *
 * @var yii\web\View $this
 */

namespace Tests\App\Http\Action\Profile;

use App\Http\Action\Profile\NewsAction;
use App\Http\Middleware\AuthMiddleware;
use App\Http\Middleware\NotFoundHandler;
use PHPUnit\Framework\TestCase;
use Tests\App\Dummy\Repository\News;
use Tests\App\Http\Action\BaseAction;
use Zend\Diactoros\ServerRequest;

class NewsActionTest extends BaseAction
{
  public function testNews()
  {
    $data = [
      'title' => 'title',
      'content' => 'content',
    ];
    $action = new NewsAction($this->renderer, new News($data));

    $request = (new ServerRequest())
      ->withAttribute(AuthMiddleware::ATTRIBUTE, 1);

    $response = $action($request);

    self::assertEquals(200, $response->getStatusCode());
    $content = $response->getBody()->getContents();
    $this->assertContains("Logout", $content);
    $this->assertContains("Latest News", $content);
    $this->assertContains("title", $content);
    $this->assertContains("content", $content);
  }
}
