<?php

namespace App\Http\Action\Profile;

use App\Entities\User;
use App\Http\Middleware\AuthMiddleware;
use App\ReadModel\ClothesReadRepository;
use App\ReadModel\NewsRepository;
use App\ReadModel\TaskReadRepository;
use App\ReadModel\UserReadRepository;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\DomCrawler\Crawler;
use Zend\Diactoros\Response\HtmlResponse;

class CabinetAction
{
  private $template;
  /**
   * @var UserReadRepository
   */
  private $repository;
  /**
   * @var NewsRepository
   */
  private $news;
  /**
   * @var TaskReadRepository
   */
  private $task;

  public function __construct(
    TemplateRenderer $template,
    UserReadRepository $repository,
    NewsRepository $news,
    TaskReadRepository $task
  )
  {
    $this->template = $template;
    $this->repository = $repository;
    $this->news = $news;
    $this->task = $task;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    $userId = $request->getAttribute(AuthMiddleware::ATTRIBUTE);
    if (!$user = new User($this->repository->findById((int)$userId))) {
      return $next($request);
    }

    $images = $this->repository->findImagesByUserId((int)$userId, 4);
    $tasks = $this->task->all((int)$userId, 3);
    return new HtmlResponse($this->template->render('app/cabinet', [
      'user' => $user,
      'images' => $images,
      'news' => $this->news->getLatestNews(),
      'tasks' => $tasks
    ]));
  }
}
