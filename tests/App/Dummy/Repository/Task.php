<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Tests\App\Dummy\Repository;

use App\ReadModel\TaskReadRepository;
use Framework\Db\DBInterface;
use Illuminate\Support\Collection;

class Task extends TaskReadRepository
{
  /**
   * @var array
   */
  private $data;

  public function __construct(DBInterface $DB, array $data = [])
  {
    parent::__construct($DB);
    $this->data = $data;
  }

  public function all($userId, $limit = null): Collection
  {
    return new Collection($this->data);
  }

}