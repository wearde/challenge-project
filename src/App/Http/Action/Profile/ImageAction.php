<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Entities\User;
use App\Http\Middleware\AuthMiddleware;
use App\ReadModel\UserReadRepository;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

class ImageAction
{
  private $template;
  /**
   * @var UserReadRepository
   */
  private $repository;

  public function __construct(TemplateRenderer $template, UserReadRepository $repository)
  {
    $this->template = $template;
    $this->repository = $repository;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    $userId = $request->getAttribute(AuthMiddleware::ATTRIBUTE);

    if(!$user = new User($this->repository->findById((int)$userId))) {
      return $next($request);
    }

    $images = $this->repository->findImagesByUserId((int)$userId);

    return new HtmlResponse($this->template->render('app/profile/image', [
      'user' => $user,
      'images' => $images,
    ]));
  }
}