<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Http\Middleware\AuthMiddleware;
use App\Services\PhotoService;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class ImageRemoveAction
{
  /**
   * @var PhotoService
   */
  private $service;

  public function __construct( PhotoService $service)
  {
    $this->service = $service;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    $userId = $request->getAttribute(AuthMiddleware::ATTRIBUTE);
    $images = $this->service->remove((int)$request->getParsedBody()['imageId'], $userId);

    return new JsonResponse([
      'status' => true,
      'images' => $images->jsonSerialize()
    ]);

  }
}