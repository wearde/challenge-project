<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Services;

use App\Core\Config;
use App\Entities\UserImage;
use App\Forms\PhotoForm;
use App\ReadModel\PhotoReadRepository;
use Framework\Db\DBInterface;
use Gumlet\ImageResize;

class PhotoService
{
  /**
   * @var PhotoReadRepository
   */
  private $repository;
  /**
   * @var Config
   */
  private $config;
  /**
   * @var DBInterface
   */
  private $DB;

  public function __construct(PhotoReadRepository $repository, DBInterface $DB, Config $config)
 {
   $this->repository = $repository;
   $this->config = $config;
   $this->DB = $DB;
 }

 public function upload(PhotoForm $form)
 {
   try {
     $photo = new UserImage($this->config, $this->DB->connection());
     $photo->insert(new PhotoForm($form->file, (int)$form->user_id));

   }catch (\Throwable $exception) {
    // some log
   }
   return $this->repository->allByUserId($form->user_id);
 }

  public function remove(int $imageId, int $userId)
  {
    $this->DB->connection()->table('user_pictures')->where('id', $imageId)->delete();
    return $this->repository->allByUserId($userId);
  }
}