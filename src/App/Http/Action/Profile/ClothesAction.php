<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\ReadModel\ClothesReadRepository;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class ClothesAction
{
  private $template;
  /**
   * @var ClothesReadRepository
   */
  private $repository;

  public function __construct(TemplateRenderer $template, ClothesReadRepository $repository)
  {
    $this->template = $template;
    $this->repository = $repository;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    return new JsonResponse([
      'data' => $this->repository->get(),
      'status' => true
    ]);

  }
}