const gulp = require('gulp');

// style paths
const files = 'dist/*.js';
const appPath = '../../public/assets/js';

gulp.task('js', () => {
  gulp
    .src(files)
    .pipe(gulp.dest(appPath));
});

gulp.task('watch', () => {
  gulp.watch(files, ['js']);
});
