import api from '../api/images';
import * as types from '../types/images';

const state = {
  images: [],
  loading: false,
  loaded: false,
  errors: null,
};

const getters = {
  getImages(state) {
    return state.images;
  },
  [types.LOADED](state) {
    return state.loaded;
  },
  [types.LOADING](state) {
    return state.loading;
  },
  [types.ERROR](state) {
    return state.errors;
  },
};

const actions = {
  async load({commit}, data) {
    commit(types.LOADING, true);
    try {
      const fetchData = await api.loadImages(data);
      if (fetchData) {
        commit(types.ERROR, null);
        commit(types.FETCH_DATA, fetchData.images);
        commit(types.LOADING, false);
        commit(types.LOADED, true);
      } else {
        commit(types.LOADING, false);
        commit(types.ERROR, 'NOT FOUND DATA');
        commit(types.LOADED, true);
      }
    } catch (e) {
      console.log(e);
      const {message} = JSON.parse(e.error);
      const error = message || 'Bad request to server, try again later';
      commit(types.LOADING, false);
      commit(types.ERROR, error);
      commit(types.LOADED, true);
      throw new Error(error);
    }
  },
  async add({commit}, data) {
    console.log(data);
    commit(types.LOADING, true);
    commit(types.ERROR, null);
    commit(types.FETCH_DATA, data);
    commit(types.LOADING, false);
    commit(types.LOADED, true);
  },
  async remove({commit}, data) {
    commit(types.LOADING, true);
    try {
      const fetchData = await api.removeImage(data);
      if (fetchData) {
        commit(types.ERROR, null);
        commit(types.FETCH_DATA, fetchData.images);
        commit(types.LOADING, false);
        commit(types.LOADED, true);
      } else {
        commit(types.LOADING, false);
        commit(types.ERROR, 'NOT FOUND DATA');
        commit(types.LOADED, true);
      }
    } catch (e) {
      console.log(e);
      const {message} = JSON.parse(e.error);
      const error = message || 'Bad request to server, try again later';
      commit(types.LOADING, false);
      commit(types.ERROR, error);
      commit(types.LOADED, true);
      throw new Error(error);
    }
  },
};

const mutations = {
  [types.LOADING](state, payload) {
    state.loading = payload;
  },
  [types.LOADED](state, payload) {
    state.loaded = payload;
  },
  [types.ERROR](state, error) {
    state.errors = error;
  },
  [types.FETCH_DATA](state, images) {
    state.images = images;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
