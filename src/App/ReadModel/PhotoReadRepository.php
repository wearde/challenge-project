<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\ReadModel;

use Framework\Db\DBInterface;

class PhotoReadRepository
{
  /**
   * @var DBInterface
   */
  private $DB;

  public function __construct(DBInterface $DB)
  {
    $this->DB = $DB;
  }

  public function allByUserId($userId, $limit = null)
  {
    return $this->DB->connection()
      ->table('user_pictures')
      ->where('user_id', $userId)
      ->limit($limit)
      ->orderBy('id', 'desc')
      ->get();
  }
}