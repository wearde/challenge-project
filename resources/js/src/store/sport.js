import listTeams from './modules/listTeams';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        listTeams,
    },
});
