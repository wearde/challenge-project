<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Entities\User;
use App\Http\Middleware\AuthMiddleware;
use App\ReadModel\NewsRepository;
use App\ReadModel\SportReadRepository;
use App\ReadModel\UserReadRepository;
use Framework\Template\TemplateRenderer;
use League\Csv\Exception;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;

class SportTeamAction
{
  private $template;
  /**
   * @var SportReadRepository
   */
  private $repository;

  public function __construct(TemplateRenderer $template, SportReadRepository $repository)
  {
    $this->template = $template;
    $this->repository = $repository;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    $body = $request->getQueryParams();
    return new JsonResponse([
      'error' => '',
      'data' => $this->repository->findByTerm($body['term']),
      'status' => true
    ]);

  }
}