<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\ReadModel;

class ClothesReadRepository
{
  public function get()
  {
    $data = (array)json_decode($this->fetch());
    return array_map(function ($el) {
      return $el->clothe;
    }, $data['payload']);
  }

  private function fetch()
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, 'https://therapy-box.co.uk/hackathon/clothing-api.php?username=swapni');
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
}