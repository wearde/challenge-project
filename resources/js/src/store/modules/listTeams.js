import api from '../api/sport';
import * as types from '../types/sport/loadTerm';

const state = {
  teams: [],
  loading: false,
  loaded: false,
  errors: null,
};

const getters = {
  getTeams(state) {
    return state.teams;
  },
  [types.LOADED](state) {
    return state.loaded;
  },
  [types.LOADING](state) {
    return state.loading;
  },
  [types.ERROR](state) {
    return state.errors;
  },
};

const actions = {
  async loadByTerm({commit}, data) {
    commit(types.LOADING, true);
    try {
      const fetchData = await api.loadByTerm(data);
      if (fetchData) {
        commit(types.ERROR, null);
        commit(types.FETCH_DATA, fetchData.data);
        commit(types.LOADING, false);
        commit(types.LOADED, true);
      } else {
        commit(types.LOADING, false);
        commit(types.ERROR, 'NOT FOUND DATA');
        commit(types.LOADED, true);
      }
    } catch (e) {
      console.log(e);
      const {message} = JSON.parse(e.error);
      const error = message || 'Bad request to server, try again later';
      commit(types.LOADING, false);
      commit(types.ERROR, error);
      commit(types.LOADED, true);
      throw new Error(error);
    }
  },
};

const mutations = {
  [types.LOADING](state, payload) {
    state.loading = payload;
  },
  [types.LOADED](state, payload) {
    state.loaded = payload;
  },
  [types.ERROR](state, error) {
    state.errors = error;
  },
  [types.FETCH_DATA](state, teams) {
    state.teams = teams;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
