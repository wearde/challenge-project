<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Framework\Db;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\ConnectionInterface;

interface DBInterface
{
  public function connection(): ConnectionInterface;

  public function getCapture(): Manager;
}