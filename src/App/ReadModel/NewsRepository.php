<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\ReadModel;

use App\ReadModel\Views\NewsView;

class NewsRepository
{

  public function getLatestNews($full = false):? NewsView
  {
    $url = 'http://feeds.bbci.co.uk/news/rss.xml';
    $namespace = "http://search.yahoo.com/mrss/";
    try {
      $xml = @simplexml_load_file($url);
      if (isset($xml->channel->item[0])) {
        $latest = $xml->channel->item[0];
        $image = $latest->children($namespace)->attributes();
        if ($full === true) {
          return new NewsView((string)$latest->title, (string)$latest->description, (string)$latest->link, (string)$image->url);
        }
        preg_match('/^(?>\S+\s*){1,5}/', (string)$latest->title, $match);
        $title = rtrim($match[0]);
        $content = substr((string)$latest->description, 0, 120) . ' ...';
        return new NewsView($title, $content, (string)$latest->link, (string)$image->url);
      }
    } catch (\Exception $exception) {

    }
    return null;
  }
}