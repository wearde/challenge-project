webpackJsonp([ 2 ], {
    129: function(e, r, t) {
        t(49), e.exports = t(331);
    },
    331: function(e, r, t) {
        "use strict";
        function compressArray(e) {
            for (var r = [], t = e.slice(0), o = 0; o < e.length; o++) {
                for (var n = 0, a = 0; a < t.length; a++) e[o] == t[a] && (n++, delete t[a]);
                if (n > 0) {
                    var i = new Object();
                    i.value = e[o], i.count = n, r.push(i);
                }
            }
            return r;
        }
        var o = t(332), n = function(e) {
            return e && e.__esModule ? e : {
                default: e
            };
        }(o);
        new Vue({
            delimiters: [ "${", "}" ],
            el: "#app",
            components: {
                Weather: n.default
            }
        }), window.chartColors = {
            red: "rgb(255, 99, 132)",
            orange: "rgb(255, 159, 64)",
            yellow: "rgb(255, 205, 86)",
            green: "rgb(75, 192, 192)",
            blue: "rgb(54, 162, 235)",
            purple: "rgb(153, 102, 255)",
            grey: "rgb(201, 203, 207)"
        };
        var a = {
            type: "pie",
            options: {
                responsive: !0
            }
        };
        window.onload = function() {
            var e = document.getElementById("chart-area").getContext("2d");
            window.$.ajax({
                url: "/cabinet/clothes",
                type: "GET",
                data: {},
                dataType: "json",
                success: function(r) {
                    var t = compressArray(r.data), o = t.map(function(e) {
                        return e.count;
                    }), n = t.map(function(e) {
                        return e.value;
                    });
                    a.data = {
                        labels: n,
                        datasets: [ {
                            data: o,
                            backgroundColor: [ window.chartColors.red, window.chartColors.orange, window.chartColors.yellow, window.chartColors.green, window.chartColors.blue, window.chartColors.grey, window.chartColors.purple ],
                            label: "Dataset 1"
                        } ]
                    }, window.myPie = new Chart(e, a);
                },
                error: function(e) {
                    console.error(e);
                }
            });
        };
    },
    332: function(e, r, t) {
        "use strict";
        Object.defineProperty(r, "__esModule", {
            value: !0
        });
        var o = t(64);
        r.default = {
            delimiters: [ "${", "}" ],
            template: "#v-weather-component",
            data: function() {
                return {
                    weather: {},
                    icon: "",
                    error: "",
                    loading: !1
                };
            },
            mounted: function() {
                var e = this;
                this.loading = !0, navigator.geolocation.getCurrentPosition(function(r) {
                    var t = r.coords, o = t.latitude, n = t.longitude;
                    e.load({
                        url: "http://api.openweathermap.org/data/2.5/weather?lat=" + o + "&lon=" + n + "&appid=d0a10211ea3d36b0a6423a104782130e&units=metric"
                    }).then(function(r) {
                        e.weather = r, e.loading = !1;
                    }).catch(function(r) {
                        var t = JSON.parse(r.error), o = t.message;
                        e.error = o, e.loading = !1;
                    });
                }, function(r) {
                    e.error = r.message, e.loading = !1;
                });
            },
            computed: {
                iconUrl: function() {
                    return "http://openweathermap.org/img/w/" + this.weather.weather[0].icon + ".png";
                }
            },
            methods: {
                getData: function(e) {
                    return new Promise(function(r, t) {
                        (0, o.fetch)(e, r, t);
                    });
                },
                load: function(e) {
                    return this.getData(e);
                }
            }
        };
    }
}, [ 129 ]);