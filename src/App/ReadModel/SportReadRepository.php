<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\ReadModel;

use App\Core\Config;
use League\Csv\Reader;
use League\Csv\Statement;

class SportReadRepository
{
  /**
   * @var Config
   */
  private $config;

  public function __construct(Config $config)
  {
    $this->config = $config;
  }

  public function findByTerm(string $term)
  {
    $dir = $this->config->getConfig('dataDir') . '/';
    $path = $dir . 'sport.csv';
    $csv = Reader::createFromPath($path, 'r');
    $csv->setHeaderOffset(0);

    $stmt = (new Statement())->where(function (array $record) use ($term) {
      return ((strtolower(substr($record['HomeTeam'], 0, strlen($term))) === strtolower($term))
          || (strtolower(substr($record['AwayTeam'], 0, strlen($term))) === strtolower($term)))
        && $record['FTR'] !== 'D';
    });

    $records = $stmt->process($csv);
    $result = array_unique(array_filter(array_map(function (array $record) use ($term) {
      $status = strtolower(substr($record['HomeTeam'], 0, strlen($term))) === strtolower($term)
        ? 'H'
        : 'A';
      if ($record['FTR'] === $status) {
        return $status === 'H'
          ? $record['AwayTeam']
          :$record['HomeTeam'];
      }
      return null;
    }, $records->jsonSerialize())));
    return $result;
  }
}