<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Tests\App\Http\Action\Profile;

use App\Http\Action\Profile\TaskSaveAction;
use App\Http\Middleware\AuthMiddleware;
use Tests\App\Dummy\Services\Task;
use Tests\App\Http\Action\BaseAction;
use Tests\Framework\Db\DummyDb;
use Zend\Diactoros\ServerRequest;

class TaskSaveActionTest extends BaseAction
{
  public function testStatus()
  {
    $action = new TaskSaveAction($this->renderer, new Task(new DummyDb()));

    $request = (new ServerRequest())
      ->withAttribute(AuthMiddleware::ATTRIBUTE, 1);

    $response = $action($request);
    self::assertEquals(200, $response->getStatusCode());
    self::assertJsonStringEqualsJsonString(
      json_encode(['status' => true]),
      $response->getBody()->getContents()
    );
  }

  public function testSave()
  {
    $action = new TaskSaveAction($this->renderer, new Task(new DummyDb()));

    $request = (new ServerRequest())
      ->withAttribute(AuthMiddleware::ATTRIBUTE, 1)
    ->withParsedBody(['todos' => [['title' => 'title', 'status' => true]]]);

    $response = $action($request);
    self::assertEquals(200, $response->getStatusCode());
    self::assertJsonStringEqualsJsonString(
      json_encode(['status' => true]),
      $response->getBody()->getContents()
    );
  }
}
