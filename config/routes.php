<?php

use App\Http\Action;

/** @var \Framework\Http\Application $app */

$app->get('home', '/', Action\HelloAction::class);
$app->post('login', '/login', Action\Auth\LoginAction::class);
$app->any('register', '/register', Action\Auth\RegisterAction::class);
$app->post('logout', '/logout', Action\Auth\LogoutAction::class);
$app->get('cabinet', '/cabinet', Action\Profile\CabinetAction::class);
$app->get('cabinet_photos', '/cabinet/photos', Action\Profile\ImageAction::class);
$app->post('cabinet_photos_upload', '/cabinet/photos/upload', Action\Profile\ImageUploadAction::class);
$app->get('cabinet_photos_load', '/cabinet/photos/load', Action\Profile\ImageLoadAction::class);
$app->post('cabinet_photos_remove', '/cabinet/photos/remove', Action\Profile\ImageRemoveAction::class);
$app->get('cabinet_news', '/cabinet/news', Action\Profile\NewsAction::class);
$app->get('cabinet_sport', '/cabinet/sport', Action\Profile\SportAction::class);
$app->get('cabinet_sport_team', '/cabinet/sport/team', Action\Profile\SportTeamAction::class);
$app->get('cabinet_task', '/cabinet/task', Action\Profile\TaskAction::class);
$app->get('cabinet_task_load', '/cabinet/task/load', Action\Profile\TaskLoadAction::class);
$app->post('cabinet_task_save', '/cabinet/task/save', Action\Profile\TaskSaveAction::class);
$app->get('cabinet_clothes', '/cabinet/clothes', Action\Profile\ClothesAction::class);
