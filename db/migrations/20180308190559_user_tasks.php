<?php


use Phinx\Migration\AbstractMigration;

class UserTasks extends AbstractMigration
{
  public function up()
  {
    $users = $this->table('user_tasks');
    $users->addColumn('user_id', 'integer')
      ->addColumn('title', 'string', ['limit' => 265])
      ->addColumn('status', 'integer', ['limit' => 1])
      ->addColumn('created', 'datetime', ['null' => true])
      ->addColumn('updated', 'datetime', ['null' => true])
      ->save();
  }

  public function down()
  {
    $this->dropTable('user_tasks');
  }
}
