<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Entities;

use App\Core\Config;
use App\Forms\PhotoForm;
use Framework\Db\Model;
use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use Illuminate\Database\ConnectionInterface;

/**
 * Class UserImage
 * @property $id
 * @property $user_id
 * @property $image_name
 * @property $image_link
 * @package App\Entities
 */
class UserImage extends Model
{
  /**
   * @var ConnectionInterface
   */
  private $connection;
  /**
   * @var Config
   */
  private $config;

  public function __construct(Config $config, ConnectionInterface $connection = null)
 {
   parent::__construct($connection);
   $this->connection = $connection;
   $this->config = $config;
 }
 public function insert(PhotoForm $form)
 {
   $this->connection->table('user_pictures')->insert([
     'user_id' => $form->user_id,
     'image_name' => $form->image_name,
     'image_link' => $form->image_link,
     'image_origin' => $form->image_original
   ]);
   // @todo may be better do it wit events
   $dir = $this->config->getConfig('imagesDir') . '/';
   $imagePath = $dir . $form->image_name;
   $form->file->moveTo($imagePath);
   try {
     $image = new ImageResize($imagePath);
     $image->crop(400, 300);
     $image->save($dir . 'thumbs/' . $form->image_name, null, 100);
   } catch (ImageResizeException $e) {
     //todo write same log needs  logger but is another story :)
   }
 }
}