<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Forms\PhotoForm;
use App\Http\Middleware\AuthMiddleware;
use App\Services\PhotoService;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class ImageUploadAction
{
  private $template;
  /**
   * @var PhotoService
   */
  private $service;

  public function __construct(TemplateRenderer $template, PhotoService $service)
  {
    $this->template = $template;
    $this->service = $service;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    $file = $request->getUploadedFiles()['files'];
    $userId = $request->getAttribute(AuthMiddleware::ATTRIBUTE);
    $images = $this->service->upload(new PhotoForm($file, $userId));

    return new JsonResponse([
      'status' => true,
      'images' => $images->jsonSerialize()
    ]);

  }
}