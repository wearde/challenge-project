// import {ImageUploader} from 'vue-image-upload-resize';
import ImagesList from './components/ImageList';
import store from './store/image';
Vue.config.debug = true;
Vue.config.devtools = true;

new Vue({
    delimiters: ['${', '}'],
    el: '#image',
    data: {},
    store,
    components: {
        ImagesList,
    },
});
