<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Tests\App\Dummy\Repository;

use App\ReadModel\NewsRepository;
use App\ReadModel\Views\NewsView;

class News extends NewsRepository
{
  private $data;

  public function __construct(array $data)
  {
    $this->data = $data;
  }

  public function getLatestNews($full = false):? NewsView
  {
    return new NewsView($this->data['title'], $this->data['content']);
  }
}