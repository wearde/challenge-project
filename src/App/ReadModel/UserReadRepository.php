<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\ReadModel;

use Framework\Db\DBInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class UserReadRepository
{
  /**
   * @var DBInterface
   */
  private $DB;

  public function __construct(DBInterface $DB)
  {
    $this->DB = $DB;
  }

  public function findByUsername($name): Collection
  {
    return $this->DB->connection()
      ->table('users')
      ->where('username', $name)
      ->get();
  }

  public function findByEmail($email): Collection
  {
    return $this->DB->connection()
      ->table('users')
      ->where('email', $email)
      ->get();
  }

  public function findById(int $id): Collection
  {
    return $this->DB->connection()
      ->table('users')
      ->where('id', $id)
      ->get();
  }

  public function findImagesByUserId(int $id, $limit = null): Collection
  {
    return $this->DB->connection()
      ->table('user_pictures')
      ->where('user_id', $id)
      ->limit($limit)
      ->get();
  }
}