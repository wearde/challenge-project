import VueCoreImageUpload from '../library/vuePictureInput';
import * as types from '../store/types/images/index';

export default {
    delimiters: ['${', '}'],
    template: '#v-images-list-component',
    data() {
        return {};
    },
    components: {
        VueCoreImageUpload,
    },
    methods: {
        imageuploaded(res) {
            this.$store.dispatch('add', res.images);
        },
        handleRemove(imageId) {
            this.$store.dispatch('remove', {
                url: '/cabinet/photos/remove',
                type: 'POST',
                data: {
                    imageId,
                },
            });
        },
    },
    mounted() {
        this.$store.dispatch('load', {
            url: '/cabinet/photos/load',
        });
    },
    computed: {
        images() {
            return this.$store.getters.getImages;
        },
        loading() {
            return this.$store.getters[types.LOADING];
        },
        loaded() {
            return this.$store.getters[types.LOADED];
        },
    },
};
