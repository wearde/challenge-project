const gulp = require('gulp');
const sass = require('gulp-sass');

// style paths
const sassFiles = 'sass/**/*.scss';
const appPath = '../../public/assets/css';

gulp.task('styles', () => {
  gulp
    .src(sassFiles)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(appPath));
});

gulp.task('watch', () => {
  gulp.watch(sassFiles, ['styles']);
});
