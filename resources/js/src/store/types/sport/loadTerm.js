export const LOADED = 'loadTerm/LOADED';
export const LOADING = 'loadTerm/LOADING';
export const FETCH_DATA = 'loadTerm/FETCH_DATA';
export const ERROR = 'loadTerm/ERROR';
