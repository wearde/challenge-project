import * as types from '../../store/types/sport/loadTerm';

export default {
    delimiters: ['${', '}'],
    template: '#v-sport-search',
    data() {
        return {
            teamName: '',
        };
    },
    computed: {
        goods() {
            return this.$store.getters[types.FETCH_DATA];
        },
        loading() {
            return this.$store.getters[types.LOADING];
        },
        loaded() {
            return this.$store.getters[types.LOADED];
        },
    },
    methods: {
        selectHandler(e) {
            if (this.teamName.length > 2) {
                this.$store
                    .dispatch('loadByTerm', {
                        url: '/cabinet/sport/team',
                        data: {
                            term: this.teamName,
                        },
                    })
                    .then(() => {
                        e.target.focus();
                    });
            }
        },
    },
};
