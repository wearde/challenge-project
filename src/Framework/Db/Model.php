<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Framework\Db;

use Illuminate\Database\ConnectionInterface;

class Model
{
  /**
   * @var ConnectionInterface
   */
  private $connection;

  public $attributes = [];

  public function __construct(ConnectionInterface $connection = null)
  {
    $this->connection = $connection;
  }

  public function setAttributes(array $values)
  {
    $this->attributes = $values;
    if (is_array($this->attributes)) {
      foreach ($this->attributes as $name => $value) {
        $this->$name = $value;
      }
    }
  }

  /**
   * @param callable $function
   * @throws \Throwable
   */
  public function save(callable $function)
  {
    $this->connection->transaction(function () use ($function) {
      $function();
    });
  }
}