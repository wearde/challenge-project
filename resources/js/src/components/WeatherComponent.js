import {fetch} from '../api';

export default {
    delimiters: ['${', '}'],
    template: '#v-weather-component',
    data() {
        return {
            weather: {},
            icon: '',
            error: '',
            loading: false,
        };
    },
    mounted() {
        const apiId = 'd0a10211ea3d36b0a6423a104782130e';
        this.loading = true;
        navigator.geolocation.getCurrentPosition(
            position => {
                const {latitude, longitude} = position.coords;
                this.load({
                    url: `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${apiId}&units=metric`,
                })
                    .then(data => {
                        this.weather = data;
                        this.loading = false;
                    })
                    .catch(e => {
                        const {message} = JSON.parse(e.error);
                        this.error = message;
                        this.loading = false;
                    });
            },
            err => {
                this.error = err.message;
                this.loading = false;
            }
        );
    },
    computed: {
        iconUrl() {
            return (
                'http://openweathermap.org/img/w/' +
        this.weather.weather[0].icon +
        '.png'
            );
        },
    },
    methods: {
        getData(data) {
            return new Promise((resolve, reject) => {
                fetch(data, resolve, reject);
            });
        },
        load(data) {
            return this.getData(data);
        },
    },
};
