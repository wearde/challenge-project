<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Forms;

use Zend\Diactoros\UploadedFile;

class RegisterForm
{
  /**
   * @var string
   */
  public $password;
  /**
   * @var string
   */
  public $username;
  /**
   * @var string
   */
  public $email;
  /**
   * @var string
   */
  public $repeat;

  /**
   * @var UploadedFile
   */
  public $image;

  private $errors = [];

  private $allowedTypes = [
    'image/gif', 'image/png', 'image/jpeg', 'image/bmp', 'image/webp'
  ];

  public function create(string $username, string $email, string $password, string $repeat)
  {
    $this->password = $password;
    $this->username = $username;
    $this->email = $email;
    $this->repeat = $repeat;
  }

  public function setImage(UploadedFile $image): self
  {
    $this->image = $image;
    return $this;
  }

  public function validate(): bool
  {
    $this->empty();
    $this->comparePasswords();
    $this->mineType();
    if ($this->errors) {
      return false;
    }
    return true;
  }

  private function comparePasswords(): bool
  {
    if ($this->password !== $this->repeat) {
      $this->errors['confirm'] = 'Confirm password needs to be same';
      return false;
    }
    return true;
  }

  private function mineType(): bool
  {
    if($this->image->getClientFilename() && !in_array($this->image->getClientMediaType(), $this->allowedTypes)){
      $this->errors['image'] = 'Allowed just images';
      return false;
    }
    return true;
  }

  /**
   * @return array
   */
  public function getErrors(): array
  {
    return $this->errors;
  }

  private function empty()
  {
    if(!$this->username){
      $this->errors['username'] = 'Empty username';
    }
    if(!$this->email){
      $this->errors['email'] = 'Empty email';
    }
    if(!$this->password){
      $this->errors['password'] = 'Empty password';
    }
  }
}