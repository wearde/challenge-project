<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Auth;

use App\Forms\LoginForm;
use App\Helpers\Auth;
use App\Http\Middleware\AuthMiddleware;
use App\Services\AuthService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Uri;
use Zend\Session\Container;

class LoginAction
{
  /**
   * @var AuthService
   */
  private $service;
  /**
   * @var Container
   */
  private $session;

  public function __construct(AuthService $service, Container $session)
  {
    $this->service = $service;
    $this->session = $session;
  }

  public function __invoke(ServerRequestInterface $request)
  {
    $body = $request->getParsedBody();
    try {
      if (!$user = $this->service->auth(new LoginForm($body['login'], $body['password']))) {
        return new RedirectResponse(new Uri('/'));
      }
    }catch (\Exception $exception) {
      return new RedirectResponse(new Uri('/'));
    }
    $this->session->offsetSet(AuthMiddleware::ATTRIBUTE, $user->id);
    return new RedirectResponse(new Uri('/cabinet'));
  }
}