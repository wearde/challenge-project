<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Framework\Db;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\ConnectionInterface;

class DB implements DBInterface
{
  /**
   * @var Manager
   */
  private $capture;

  public function __construct(Manager $capture)
  {
    $this->capture = $capture;
  }

  public function connection(): ConnectionInterface
  {
    return $this->capture->getConnection();
  }

  /**
   * @return Manager
   */
  public function getCapture(): Manager
  {
    return $this->capture;
  }
}