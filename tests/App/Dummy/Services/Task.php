<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Tests\App\Dummy\Services;

use App\Services\TaskService;

class Task extends TaskService
{
  public function save(array $forms, $userId): void
  {

  }
}