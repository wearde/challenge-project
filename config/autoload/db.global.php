<?php

use Framework\Db\DB;
use Framework\Db\DBInterface;
use Illuminate\Database\Capsule\Manager;
use Psr\Container\ContainerInterface;

return [
  'dependencies' => [
    'factories' => [
      DBInterface::class => function (ContainerInterface $container) {
        $config = $container->get('config')['connection'];
        $capsule = new Manager();
        $capsule->addConnection([
          'driver' => $config['driver'],
          'host' => $config['host'],
          'port' => $config['port'],
          'database' => $config['database'],
          'username' => $config['username'],
          'password' => $config['password'],
          'charset' => 'utf8',
          'collation' => 'utf8_unicode_ci',
          'prefix' => '',
        ]);
        $capsule->setAsGlobal();
        return new DB($capsule);
      },
    ],
  ],
  'connection' => [
    'driver' => env('DB_CONNECTION', 'mysql'),
    'host' => env('DB_HOST', 'localhost'),
    'database' => env('DB_DATABASE', 'database'),
    'username' => env('DB_USERNAME', 'username'),
    'password' => env('DB_PASSWORD', 'password'),
    'port' => env('DB_PORT', 3306),
  ],
];