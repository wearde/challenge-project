<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Forms;

class LoginForm
{
  /**
   * @var string
   */
  public $login;
  /**
   * @var string
   */
  public $password;

  public function __construct(string $login, string $password)
  {
    $this->login = $login;
    $this->password = $password;
  }
}