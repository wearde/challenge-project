<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Tests\App\Core;

use App\Core\Config;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{

  public function testGetConfig()
  {
    $data = [
      'key' => 'value'
    ];
    $config = new Config($data);
    self::assertEquals('value', $config->getConfig('key'));
  }
}
