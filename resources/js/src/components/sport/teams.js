import * as types from '../../store/types/sport/loadTerm';

export default {
    delimiters: ['${', '}'],
    template: '#v-sport-teams',
    computed: {
        goods() {
            return this.$store.getters.getTeams;
        },
        loading() {
            return this.$store.getters[types.LOADING];
        },
        loaded() {
            return this.$store.getters[types.LOADED];
        },
    },
};
