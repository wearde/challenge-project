export const fetch = (
    {url, data = {}, type = 'GET', dataType = 'json'},
    resolve,
    reject
) => {
    window.$.ajax({
        url,
        type,
        data,
        dataType,
        success(json) {
            resolve(json);
        },
        error: function error(xhr) {
            reject({
                status: xhr.status,
                error: xhr.responseText,
            });
        },
    });
};
