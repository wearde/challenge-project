<?php

namespace App\Http\Middleware;

use App\Services\AuthService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Uri;
use Zend\Session\Container;

class AuthMiddleware
{
    public const ATTRIBUTE = '_user';
  /**
   * @var Container
   */
  private $session;

  public function __construct(Container $session)
    {
      $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        if ($id = $this->session->offsetGet(self::ATTRIBUTE)) {
          return $next($request->withAttribute(self::ATTRIBUTE, $id));
        }

      return new RedirectResponse(new Uri('/'));
    }
}
