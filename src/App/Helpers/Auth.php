<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Helpers;

use RuntimeException;

class Auth
{
  public static function generate(string $value, int $cost = 12): string
  {
    $hash = password_hash($value, PASSWORD_BCRYPT, [
      'cost' => $cost,
    ]);

    if ($hash === false) {
      throw new RuntimeException('Bcrypt hashing not supported.');
    }

    return $hash;
  }

  public static function check(string $value, string $hashedValue = ''): bool
  {
    if (strlen($hashedValue) === 0) {
      return false;
    }
    return password_verify($value, $hashedValue);
  }
}