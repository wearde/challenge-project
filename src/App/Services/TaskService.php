<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Services;

use App\Forms\TaskForm;
use Framework\Db\DBInterface;

class TaskService
{
  /**
   * @var DBInterface
   */
  private $DB;

  public function __construct(DBInterface $DB)
  {
    $this->DB = $DB;
  }

  /**
   * @param TaskForm[] $forms
   * @param $userId
   * @throws \Throwable
   */
  public function save(array $forms, $userId): void
  {
    $this->DB->connection()->transaction(function () use($forms, $userId) {
      $this->DB->connection()
        ->table('user_tasks')
        ->where('user_id', $userId)
        ->delete();
      foreach ($forms as $form) {
        $this->DB->connection()->table('user_tasks')->insert([
          'user_id' => $userId,
          'title' => $form->title,
          'status' => (int)$form->status
        ]);
      }
    });
  }

}