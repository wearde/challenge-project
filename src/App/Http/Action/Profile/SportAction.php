<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Entities\User;
use App\Http\Middleware\AuthMiddleware;
use App\ReadModel\NewsRepository;
use App\ReadModel\UserReadRepository;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

class SportAction
{
  private $template;
  /**
   * @var NewsRepository
   */
  private $repository;

  public function __construct(TemplateRenderer $template, NewsRepository $repository)
  {
    $this->template = $template;
    $this->repository = $repository;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    return new HtmlResponse($this->template->render('app/profile/sport', [
      'news' => $this->repository->getLatestNews(true),
    ]));
  }
}