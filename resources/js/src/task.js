import {fetch} from './api';

Vue.config.debug = true;
Vue.config.devtools = true;

const STORAGE_KEY = 'todos-vuejs-2.0';

function getData(data) {
  return new Promise((resolve, reject) => {
    fetch(data, resolve, reject);
  });
}
function load(data) {
  return getData(data);
}

function save(data) {
  return getData(data);
}

function serverSave(todos, status) {
  if (status) {
    load({
      url: '/cabinet/task/save',
      type: 'POST',
      data: {
        todos,
      },
    });
  }
}

const todoStorage = {
  async query() {
    const data = await load({
      url: '/cabinet/task/load',
    });
    return data.data;
  },
  save(todos, status) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(todos));
    serverSave(todos, status);
  },
};

// visibility filters
const filters = {
  all(todos) {
    return todos;
  },
  active(todos) {
    return todos.filter(todo => {
      return !todo.status;
    });
  },
  completed(todos) {
    return todos.filter(todo => {
      return todo.status;
    });
  },
};

// app Vue instance
const app = new Vue({
  delimiters: ['${', '}'],
  el: '#task',
  data: {
    todos: [],
    newTodo: '',
    editedTodo: null,
    visibility: 'all',
    changed: false,
  },

  // watch todos change for localStorage persistence
  watch: {
    todos: {
      handler(todos) {
        this.changed = true;
      },
      deep: true,
    },
  },

  mounted() {
    todoStorage.query().then(data => {
      todoStorage.uid = data.length + 1;
      this.todos = data;
    });
  },

  computed: {
    filteredTodos() {
      return filters[this.visibility](this.todos);
    },
    remaining() {
      return filters.active(this.todos).length;
    },
  },

  // methods that implement data logic.
  // note there's no DOM manipulation here at all.
  methods: {
    addTodo() {
      const value = this.newTodo && this.newTodo.trim();
      if (!value) {
        return;
      }
      this.todos.push({
        id: todoStorage.uid++,
        title: value,
        staus: 0,
      });
      this.newTodo = '';
    },

    removeTodo(todo) {
      this.todos.splice(this.todos.indexOf(todo), 1);
    },

    editTodo(todo) {
      this.changed = false;
      this.beforeEditCache = todo.title;
      this.editedTodo = todo;
    },
    handelToggle(id) {
      this.todos.map(todo => {
        if (todo.id === id) {
          todo.status = Number(!todo.status);
        }
      });
      todoStorage.save(this.todos, true);
    },

    doneEdit(todo) {
      if (!this.editedTodo) {
        return;
      }

      this.editedTodo = null;
      todo.title = todo.title.trim();
      if (!todo.title) {
        this.removeTodo(todo);
      }
      todoStorage.save(this.todos, this.changed);
    },

    cancelEdit(todo) {
      this.editedTodo = null;
      todo.title = this.beforeEditCache;
    },
  },

  directives: {
    'todo-focus'(el, binding) {
      if (binding.value) {
        el.focus();
      }
    },
  },
});
