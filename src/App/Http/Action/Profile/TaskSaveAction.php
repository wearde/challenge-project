<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Forms\TaskForm;
use App\Http\Middleware\AuthMiddleware;
use App\Services\TaskService;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class TaskSaveAction
{
  private $template;
  /**
   * @var TaskService
   */
  private $service;

  public function __construct(TemplateRenderer $template, TaskService $service)
  {
    $this->template = $template;
    $this->service = $service;
  }

  public function __invoke(ServerRequestInterface $request)
  {
    $userId = $request->getAttribute(AuthMiddleware::ATTRIBUTE);
    $body = $request->getParsedBody();
    $todos = $body['todos'];
    if ($todos) {
      $forms = [];
      foreach ($todos as $todo) {
        $forms[] = new TaskForm($todo['title'], (int)$todo['status']);
      }
      try {
        $this->service->save($forms, $userId);
      } catch (\Throwable $e) {
        // log
      }
    }

    return new JsonResponse([
      'status' => true
    ]);
  }
}