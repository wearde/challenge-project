<?php

use Phinx\Migration\AbstractMigration;

class UserPictures extends AbstractMigration
{
  public function up()
  {
    $users = $this->table('user_pictures');
    $users->addColumn('user_id', 'integer')
      ->addColumn('image_name', 'string', ['limit' => 265])
      ->addColumn('image_link', 'string', ['limit' => 256])
      ->addColumn('image_origin', 'string', ['limit' => 256])
      ->addColumn('created', 'datetime', ['null' => true])
      ->addColumn('updated', 'datetime', ['null' => true])
      ->save();
  }

  public function down()
  {
    $this->dropTable('user_pictures');
  }
}
