<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\ReadModel\Views;

class NewsView
{
  public $title;
  public $content;
  public $image;
  public $link;

  public function __construct($title, $content, $link = '', $image = '')
  {
    $this->title = $title;
    $this->content = $content;
    $this->link = $link;
    $this->image = $image;
  }
}