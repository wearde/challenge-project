<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Core;

class Config
{
  /**
   * @var array
   */
  private $config;

  public function __construct($config = [])
  {
    $this->config = $config;
  }

  /**
   * @param $value
   * @return null|string
   */
  public function getConfig($value): ?string
  {
    return isset($this->config[$value]) ? $this->config[$value] : '';
  }
}