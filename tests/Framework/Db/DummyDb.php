<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Tests\Framework\Db;

use Framework\Db\DBInterface;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\ConnectionInterface;

class DummyDb implements DBInterface
{

  public function connection(): ConnectionInterface
  {
    // TODO: Implement connection() method.
  }

  public function getCapture(): Manager
  {
    // TODO: Implement getCapture() method.
  }
}