<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Forms;

class TaskForm
{
  /**
   * @var string
   */
  public $title;
  /**
   * @var int
   */
  public $status;

  public function __construct(string $title, int $status)
  {
    $this->title = $title;
    $this->status = $status;
  }
}