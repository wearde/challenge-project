<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Auth;

use App\Http\Middleware\AuthMiddleware;
use App\Services\AuthService;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Uri;
use Zend\Session\Container;

class LogoutAction
{
  /**
   * @var AuthService
   */
  private $service;
  /**
   * @var Container
   */
  private $session;

  public function __construct(AuthService $service, Container $session)
  {
    $this->service = $service;
    $this->session = $session;
  }

  public function __invoke(ServerRequestInterface $request)
  {
    $this->session->offsetUnset(AuthMiddleware::ATTRIBUTE);
    return new RedirectResponse(new Uri('/'));
  }
}