<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Auth;

use App\Core\Config;
use App\Forms\RegisterForm;
use App\Services\AuthService;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Uri;
use Zend\Session\Container;

class RegisterAction
{
  /**
   * @var AuthService
   */
  private $service;
  /**
   * @var Container
   */
  private $session;
  /**
   * @var TemplateRenderer
   */
  private $template;

  public function __construct(TemplateRenderer $template, AuthService $service, Container $session)
  {
    $this->service = $service;
    $this->session = $session;
    $this->template = $template;
  }

  public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
  {
    $errors = [];
    $form = new RegisterForm();
    if ($request->getMethod() === 'POST') {
      $body = $request->getParsedBody();
      $form->create(
        $body['login'],
        $body['email'],
        $body['password'],
        $body['repeat']);
      $form->setImage($request->getUploadedFiles()['image']);
      if ($this->service->uniqUsername($form)) {
        $errors['username'] = 'This username already exist';

      }
      if ($this->service->uniqEmail($form)) {
        $errors['email'] = 'This email already exist';
      }

      if ($form->validate() && !$errors) {
        if($this->service->register($form)) {
          return new RedirectResponse(new Uri('/'));
        }
      }
      $errors = array_merge($form->getErrors(), $errors);
    }

    return new HtmlResponse($this->template->render('app/register', [
      'form' => $form,
      'errors' => $errors,
    ]));
  }
}