<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 *
 * @var yii\web\View $this
 */

namespace Tests\App\Http\Action\Profile;

use App\Http\Action\Profile\TaskLoadAction;
use App\Http\Middleware\AuthMiddleware;
use App\Http\Middleware\NotFoundHandler;
use Tests\App\Dummy\Repository\Task;
use Tests\App\Http\Action\BaseAction;
use Tests\Framework\Db\DummyDb;
use Zend\Diactoros\ServerRequest;

class TaskLoadActionTest extends BaseAction
{
  private $values;

  protected function setUp()
  {
    parent::setUp();
    $this->values = [
      [
        'created' => null,
        'id' => 1,
        'status' => 1,
        'title' => 'done',
        'updated' => null,
        'user_id' => 1,
      ]
    ];
  }

  public function testAction()
  {
    $action = new TaskLoadAction($this->renderer, new Task(new DummyDb(), $this->values));
    $request = (new ServerRequest())
      ->withAttribute(AuthMiddleware::ATTRIBUTE, 1);

    $response = $action($request, new NotFoundHandler($this->renderer));

    self::assertEquals(200, $response->getStatusCode());
    self::assertJsonStringEqualsJsonString(
      json_encode(['data' => $this->values, 'status' => true]),
      $response->getBody()->getContents()
    );
  }
}