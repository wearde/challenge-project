import images from './modules/images';
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        images,
    },
});
