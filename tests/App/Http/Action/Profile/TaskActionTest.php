<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 *
 * @var yii\web\View $this
 */

namespace Tests\App\Http\Action\Profile;

use App\Http\Action\Profile\TaskAction;
use App\Http\Middleware\AuthMiddleware;
use App\Http\Middleware\NotFoundHandler;
use Tests\App\Dummy\Repository\Task;
use Tests\App\Http\Action\BaseAction;
use Tests\Framework\Db\DummyDb;
use Zend\Diactoros\ServerRequest;

class TaskActionTest extends BaseAction
{
  public function testAction()
  {
    $action = new TaskAction($this->renderer, new Task(new DummyDb()));
    $request = (new ServerRequest())
      ->withAttribute(AuthMiddleware::ATTRIBUTE, 1);

    $response = $action($request, new NotFoundHandler($this->renderer));

    self::assertEquals(200, $response->getStatusCode());
    $content = $response->getBody()->getContents();
    $this->assertContains("Tasks", $content);
    $this->assertContains("Logout", $content);
  }
}
