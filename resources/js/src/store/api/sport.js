import {fetch} from '../../api';

export default {
    loadByTerm(data) {
        return new Promise((resolve, reject) => {
            fetch(data, resolve, reject);
        });
    },
};
