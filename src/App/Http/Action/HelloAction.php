<?php

namespace App\Http\Action;

use App\Http\Middleware\AuthMiddleware;
use App\Services\AuthService;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Uri;
use Zend\Session\Container;

class HelloAction
{
  private $template;
  /**
   * @var Container
   */
  private $session;

  public function __construct(TemplateRenderer $template, Container $session)
  {
    $this->template = $template;
    $this->session = $session;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    if ($this->session->offsetGet(AuthMiddleware::ATTRIBUTE)) {
      return new RedirectResponse(new Uri('/cabinet'));
    };

    $error = $this->session->offsetGet(AuthService::ERROR_KEY);
    $this->session->offsetUnset(AuthService::ERROR_KEY);
    return new HtmlResponse($this->template->render('app/hello', [
      'error' => $error
    ]));
  }
}
