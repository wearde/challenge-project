<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Entities;

use App\Helpers\Auth;
use Framework\Db\Model;
use Illuminate\Database\Connection;
use Illuminate\Support\Collection;
use Zend\Diactoros\UploadedFile;

/**
 * Class User
 * @property $id
 * @property $username
 * @property $email
 * @property $password
 * @package App\Entities
 */
class User extends Model
{
  /**
   * @var UploadedFile
   */
  public $image;

  public function __construct( Collection $collection, Connection $connection = null)
  {
    parent::__construct($connection);
    $this->setAttributes((array)$collection->first());
  }

  public function validatePassword($password): bool
  {
    return Auth::check($password, $this->password ?? '');
  }
}