<?php
/*** @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Http\Middleware\AuthMiddleware;
use App\ReadModel\TaskReadRepository;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class TaskLoadAction
{
  private $template;
  /**
   * @var TaskReadRepository
   */
  private $repository;

  public function __construct(TemplateRenderer $template, TaskReadRepository $repository)
  {
    $this->template = $template;
    $this->repository = $repository;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    $userId = $request->getAttribute(AuthMiddleware::ATTRIBUTE);
    return new JsonResponse([
      'data' => $this->repository->all($userId),
      'status' => true
    ]);
  }
}