<?php


use Phinx\Migration\AbstractMigration;

class Users extends AbstractMigration
{
    public function up()
    {
      $users = $this->table('users');
      $users->addColumn('username', 'string', ['limit' => 30])
        ->addColumn('password', 'string', ['limit' => 265])
        ->addColumn('password_salt', 'string', ['limit' => 256, 'null' => true])
        ->addColumn('email', 'string', ['limit' => 60])
        ->addColumn('created', 'datetime', ['null' => true])
        ->addColumn('updated', 'datetime', ['null' => true])
        ->addIndex(['username', 'email'], ['unique' => true])
        ->save();
    }

    public function down()
    {
      $this->dropTable('users');
    }
}
