<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Tests\App\Http\Action;

use App\Http\Action\Auth\LogoutAction;
use Aura\Router\RouterContainer;
use Framework\Http\Application;
use Framework\Http\Pipeline\MiddlewareResolver;
use Framework\Http\Router\AuraRouterAdapter;
use Framework\Template\Twig\Extension\RouteExtension;
use Framework\Template\Twig\TwigRenderer;
use PHPUnit\Framework\TestCase;
use Tests\Framework\Http\DummyContainer;
use Tests\Framework\Http\Pipeline\NotFoundMiddleware;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Zend\Diactoros\Response;

class BaseAction extends TestCase
{
  protected $renderer;
  /**
   * @var Application
   */
  protected $app;

  protected function setUp()
  {
    parent::setUp();
    $resolver = new MiddlewareResolver(new DummyContainer());
    $router = new AuraRouterAdapter(new RouterContainer());
    $this->app = new Application($resolver, $router, new NotFoundMiddleware(), new Response());
    $loader = new FilesystemLoader();
    try {
      $loader->addPath('templates');
    } catch (\Twig_Error_Loader $e) {
    }
    $environment = new Environment($loader);
    $environment->addExtension(new RouteExtension($router));

    $this->renderer = new TwigRenderer($environment, '.html.twig');
    $this->app->post('logout', '/logout', LogoutAction::class);
  }
}