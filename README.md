## Further info - Weather, News and Sport
#### Weather
Access to location service, preview thumbnail to show icon, current location and celsius temperature. Implement icon to indicate rainy, sunny or cloudy, name the location, show result in celcius, no internal page navigation required
#### News
Implement API as provided, preview thumbnail to show image headline. Internal page navigation to show only the latest story with image, headline and sub text
#### Sport
Use data provided. Thumbnail - show team name, internal page to have input field to insert team name. Chosen team (in input field) should produce a list of team names it has beaten in the champions league data that was made available. Ex - If input field is Juventus, list to include teams it has beaten

## Further info - Photos, tasks and Clothes
#### Photos
Pick photos from your local device to add to photo library within this module. Thumbnail to show preview of upto 4 uploaded images. Internal page to support adding new photos. Any uploaded image image to be resized to 280 * 280. Deleting not mandatory but will be a bonus
#### Tasks
preview to show top 3 tasks and status for it. Internal page to show a list of tasks, which when tapped are an input field. When user types in text, it becomes the description of the task. A status checkbox is listed next to it, where user can tap to assign complete status or tap again to revert to incomplete status
#### Clothes
data is provided for 1000 days with a column for what type of clothing did someone wer, ex raincoat, sweater, jacket etc. Your task is to show a pie chart distribution of which type of clothing was worn how often over a 100% pie chart. Ex - if raincoat was worn 200 days out of the 1000, the pie chart would show 20% for raincoat and so on.
 
 