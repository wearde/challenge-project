<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Forms;

use Psr\Http\Message\UploadedFileInterface;

class PhotoForm
{
  /**
   * @var string
   */
  public $image_name;
  /**
   * @var string
   */
  public $image_link;

  /**
   * @var string
   */
  public $image_original;
  /**
   * @var int
   */
  public $user_id;
  /**
   * @var UploadedFileInterface
   */
  public $file;

  public function __construct(UploadedFileInterface $file, int $user_id)
  {
    $this->user_id = $user_id;
    $this->image_link = '/images/thumbs/' . $file->getClientFilename();
    $this->image_name = $file->getClientFilename();
    $this->image_original = '/images/' . $file->getClientFilename();
    $this->file = $file;
  }
}