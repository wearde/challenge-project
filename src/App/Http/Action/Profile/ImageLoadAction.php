<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace App\Http\Action\Profile;

use App\Http\Middleware\AuthMiddleware;
use App\ReadModel\PhotoReadRepository;
use Framework\Template\TemplateRenderer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class ImageLoadAction
{
  private $template;
  /**
   * @var PhotoReadRepository
   */
  private $repository;

  public function __construct(TemplateRenderer $template, PhotoReadRepository $repository)
  {
    $this->template = $template;
    $this->repository = $repository;
  }

  public function __invoke(ServerRequestInterface $request, callable $next)
  {
    $userId = $request->getAttribute(AuthMiddleware::ATTRIBUTE);
    $images = $this->repository->allByUserId($userId);

    return new JsonResponse([
      'status' => true,
      'images' => $images->jsonSerialize()
    ]);

  }
}