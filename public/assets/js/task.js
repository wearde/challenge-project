webpackJsonp([ 3 ], {
    340: function(t, e, n) {
        n(49), t.exports = n(341);
    },
    341: function(t, e, n) {
        "use strict";
        function _asyncToGenerator(t) {
            return function() {
                var e = t.apply(this, arguments);
                return new Promise(function(t, n) {
                    function step(o, i) {
                        try {
                            var r = e[o](i), s = r.value;
                        } catch (t) {
                            return void n(t);
                        }
                        if (!r.done) return Promise.resolve(s).then(function(t) {
                            step("next", t);
                        }, function(t) {
                            step("throw", t);
                        });
                        t(s);
                    }
                    return step("next");
                });
            };
        }
        function getData(t) {
            return new Promise(function(e, n) {
                (0, o.fetch)(t, e, n);
            });
        }
        function load(t) {
            return getData(t);
        }
        function serverSave(t, e) {
            e && load({
                url: "/cabinet/task/save",
                type: "POST",
                data: {
                    todos: t
                }
            });
        }
        var o = n(64);
        Vue.config.debug = !0, Vue.config.devtools = !0;
        var i = {
            query: function() {
                function query() {
                    return t.apply(this, arguments);
                }
                var t = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
                    var t;
                    return regeneratorRuntime.wrap(function(e) {
                        for (;;) switch (e.prev = e.next) {
                          case 0:
                            return e.next = 2, load({
                                url: "/cabinet/task/load"
                            });

                          case 2:
                            return t = e.sent, e.abrupt("return", t.data);

                          case 4:
                          case "end":
                            return e.stop();
                        }
                    }, _callee, this);
                }));
                return query;
            }(),
            save: function(t, e) {
                localStorage.setItem("todos-vuejs-2.0", JSON.stringify(t)), serverSave(t, e);
            }
        }, r = {
            all: function(t) {
                return t;
            },
            active: function(t) {
                return t.filter(function(t) {
                    return !t.status;
                });
            },
            completed: function(t) {
                return t.filter(function(t) {
                    return t.status;
                });
            }
        };
        new Vue({
            delimiters: [ "${", "}" ],
            el: "#task",
            data: {
                todos: [],
                newTodo: "",
                editedTodo: null,
                visibility: "all",
                changed: !1
            },
            watch: {
                todos: {
                    handler: function(t) {
                        this.changed = !0;
                    },
                    deep: !0
                }
            },
            mounted: function() {
                var t = this;
                i.query().then(function(e) {
                    i.uid = e.length + 1, t.todos = e;
                });
            },
            computed: {
                filteredTodos: function() {
                    return r[this.visibility](this.todos);
                },
                remaining: function() {
                    return r.active(this.todos).length;
                }
            },
            methods: {
                addTodo: function() {
                    var t = this.newTodo && this.newTodo.trim();
                    t && (this.todos.push({
                        id: i.uid++,
                        title: t,
                        staus: 0
                    }), this.newTodo = "");
                },
                removeTodo: function(t) {
                    this.todos.splice(this.todos.indexOf(t), 1);
                },
                editTodo: function(t) {
                    this.changed = !1, this.beforeEditCache = t.title, this.editedTodo = t;
                },
                handelToggle: function(t) {
                    this.todos.map(function(e) {
                        e.id === t && (e.status = Number(!e.status));
                    }), i.save(this.todos, !0);
                },
                doneEdit: function(t) {
                    this.editedTodo && (this.editedTodo = null, t.title = t.title.trim(), t.title || this.removeTodo(t), 
                    i.save(this.todos, this.changed));
                },
                cancelEdit: function(t) {
                    this.editedTodo = null, t.title = this.beforeEditCache;
                }
            },
            directives: {
                "todo-focus": function(t, e) {
                    e.value && t.focus();
                }
            }
        });
    }
}, [ 340 ]);