import Weather from './components/WeatherComponent';

new Vue({
    delimiters: ['${', '}'],
    el: '#app',
    components: {
        Weather,
    },
});

window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)',
};

function compressArray(original) {
    var compressed = [];
    // make a copy of the input array
    var copy = original.slice(0);

    // first loop goes over every element
    for (var i = 0; i < original.length; i++) {
        var myCount = 0;
        // loop over every element in the copy and see if it's the same
        for (var w = 0; w < copy.length; w++) {
            if (original[i] == copy[w]) {
                // increase amount of times duplicate is found
                myCount++;
                // sets item to undefined
                delete copy[w];
            }
        }

        if (myCount > 0) {
            var a = new Object();
            a.value = original[i];
            a.count = myCount;
            compressed.push(a);
        }
    }

    return compressed;
}
const config = {
    type: 'pie',
    options: {
        responsive: true,
    },
};
window.onload = function() {
    const ctx = document.getElementById('chart-area').getContext('2d');
    window.$.ajax({
        url: '/cabinet/clothes',
        type: 'GET',
        data: {},
        dataType: 'json',
        success(json) {
            const d = compressArray(json.data);
            const values = d.map(el => {
                return el.count;
            });
            const labels = d.map(el=> {
                return el.value;
            });

            config.data = {
                labels: labels,
                datasets: [
                    {
                        data: values,
                        backgroundColor: [
                            window.chartColors.red,
                            window.chartColors.orange,
                            window.chartColors.yellow,
                            window.chartColors.green,
                            window.chartColors.blue,
                            window.chartColors.grey,
                            window.chartColors.purple,
                        ],
                        label: 'Dataset 1',
                    },
                ],
            };
            window.myPie = new Chart(ctx, config);
        },
        error: function error(xhr) {
          console.error(xhr);
        },
    });
};
