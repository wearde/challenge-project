import store from './store/sport';
import sportSearch from './components/sport/search';
import teams from './components/sport/teams';

Vue.config.debug = true;
Vue.config.devtools = true;

new Vue({
    delimiters: ['${', '}'],
    el: '#sport',
    store,
    components: {
        sportSearch,
        teams,
    },
});
